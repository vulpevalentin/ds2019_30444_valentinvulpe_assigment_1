package main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SpringBootApplication
public class Application {

//    @Component
//    @Order(Ordered.HIGHEST_PRECEDENCE)
//    public class MyCORSFilter implements Filter {
//
//
//        @Override
//        public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
//
//            HttpServletRequest request = (HttpServletRequest) req;
//            HttpServletResponse response = (HttpServletResponse) res;
//
//            response.setHeader("Access-Control-Allow-Origin", request.getHeader("http://localhost:4200"));
//            response.setHeader("Access-Control-Allow-Credentials", "true");
//            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
//            response.setHeader("Access-Control-Max-Age", "3600");
//            response.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With, remember-me");
//
//            chain.doFilter(req, res);
//        }
//
//        @Override
//        public void init(FilterConfig filterConfig) {
//        }
//
//        @Override
//        public void destroy() {
//        }
//
//    }


    @Configuration
    public class SecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
//            http.csrf().disable().
//
//                    authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/**").permitAll().anyRequest().authenticated()
//                    .and().httpBasic();
           // http.authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/**").authenticated().and().httpBasic();

           // http.httpBasic();
//            MyCORSFilter myCORSFilter = new MyCORSFilter();
//            http.cors().configure(http.addFilter(myCORSFilter).);
            http.csrf().disable();
        }
//
//        @Autowired
//        public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//            auth.inMemoryAuthentication().withUser("javainuse").password("{noop}p").roles("USER");
//        }

    }


//
//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("javainuse").password("{noop}password").roles("USER");
//    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
