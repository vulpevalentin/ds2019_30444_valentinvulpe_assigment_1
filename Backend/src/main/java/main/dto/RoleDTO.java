package main.dto;


import java.util.List;

public class RoleDTO {

    private Integer id;
    private String name;
    private List<UserDTO> users;

    public RoleDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    public RoleDTO(String name){
        this.name=name;
    }
    public RoleDTO(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }

    public boolean isPatient(){
        return this.getId() == 3;
    }
    public boolean isCaregiver(){
        return this.getId() == 2;
    }
    public boolean isDoctor(){
        return this.getId() == 1;
    }

}
