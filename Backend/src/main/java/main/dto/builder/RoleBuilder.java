package main.dto.builder;

import main.dto.RoleDTO;
import main.entity.Role;
import main.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;

public class RoleBuilder {


    public static RoleDTO generateDTOFromEntity(Role entity){
        return new RoleDTO(
                entity.getId(),
                entity.getName()
        );
    }

    public static Role generateEnityFromDTO(RoleDTO dto){
        return new Role(
                dto.getId(),
                dto.getName()
        );
    }
}
