package main.dto.builder;

import main.dto.PatientDTO;
import main.entity.Patient;

public class PatientBuilder {
    public static PatientDTO generateDTOFromEntity(Patient entity){
        return new PatientDTO (
                entity.getId(),
                UserBuilder.generateDTOFromEntity(entity.getUser()),
                UserBuilder.generateDTOFromEntity(entity.getCaregiver())
                //MedPlanBuilder.generateDTOFromEntity(entity.getMedPlan())
        );
    }

    public static Patient generateEnityFromDTO(PatientDTO dto){
        return new Patient(
                dto.getId(),
                UserBuilder.generateEntityFromDTO(dto.getUser()),
                UserBuilder.generateEntityFromDTO(dto.getCaregiver())
                //MedPlanBuilder.generateEntityFromDTO(dto.getMedPlan())
        );
    }
}
