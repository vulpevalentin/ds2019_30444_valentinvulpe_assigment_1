package main.dto.builder;

import main.dto.MedPlanDTO;
import main.entity.MedPlan;

import java.util.ArrayList;
import java.util.List;

public class MedPlanBuilder {
    public static MedPlanDTO generateDTOFromEntity(MedPlan entity){
        return new MedPlanDTO(
                entity.getId(),
                entity.getDateStart(),
                entity.getDateEnd(),
                entity.getDailyIntake(),
                MedicationBuilder.generateDTOFromEntity(entity.getMedication()),
                PatientBuilder.generateDTOFromEntity(entity.getPatient())
        );
    }

    public static MedPlan generateEntityFromDTO(MedPlanDTO dto){
        return new MedPlan(
                dto.getId(),
                dto.getDateStart(),
                dto.getDateEnd(),
                dto.getDailyIntake(),
                MedicationBuilder.generateEntityFromDTO(dto.getMedication()),
                PatientBuilder.generateEnityFromDTO(dto.getPatient())
        );
    }
    public static List<MedPlanDTO> generateDTOFromEntity(List<MedPlan> list){
        List<MedPlanDTO> listDto = new ArrayList<>();
        if (list != null)
            list.forEach(e -> listDto.add(generateDTOFromEntity(e)));
        return listDto;
    }
    public static List<MedPlan> generateEntityFromDTO(List<MedPlanDTO> list){
        List<MedPlan> listDto = new ArrayList<>();
        if(list != null)
            list.forEach(e -> listDto.add(generateEntityFromDTO(e)));
        return listDto;
    }
}
