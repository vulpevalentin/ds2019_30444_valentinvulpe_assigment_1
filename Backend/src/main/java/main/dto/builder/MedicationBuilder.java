package main.dto.builder;

import main.dto.MedicationDTO;
import main.entity.Medication;

import java.util.ArrayList;
import java.util.List;

public class MedicationBuilder {
    public static MedicationDTO generateDTOFromEntity(Medication entity){
        return new MedicationDTO(
                entity.getId(),
                entity.getName()
                //MedPlanBuilder.generateDTOFromEntity(entity.getMedPlans())
        );
    }

    public static Medication generateEntityFromDTO(MedicationDTO dto){
        return new Medication(
                dto.getId(),
                dto.getName()
                //MedPlanBuilder.generateEntityFromDTO(dto.getMedPlans())
        );
    }
    public static List<MedicationDTO> generateDTOFromEntity(List<Medication> list){
        List<MedicationDTO> listDto = new ArrayList<>();
        if(list != null)
            list.forEach(e -> listDto.add(generateDTOFromEntity(e)));
        return listDto;
    }
}
