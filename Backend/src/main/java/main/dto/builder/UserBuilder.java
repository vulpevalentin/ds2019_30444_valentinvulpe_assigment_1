package main.dto.builder;

import main.dto.UserDTO;
import main.entity.User;

public class UserBuilder {
    private UserBuilder(){}

    public static UserDTO generateDTOFromEntity(User entity){
        return new UserDTO(
                entity.getId(),
                entity.getName(),
                entity.getEmail(),
                entity.getAddress(),
                entity.getTelephone(),
                entity.getDateOfBirth(),
                RoleBuilder.generateDTOFromEntity(entity.getRole()));
    }

    public static User generateEntityFromDTO(UserDTO dto){
        return new User(
                dto.getId(),
                dto.getName(),
                dto.getEmail(),
                dto.getAddress(),
                dto.getTelephone(),
                dto.getDateOfBirth(),
                RoleBuilder.generateEnityFromDTO(dto.getRole())
        );
    }

//    public static UserDTO generateDTOFromView(UserViewDTO userViewDTO){
//        UserDTO dto = new UserDTO();
//
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        try {
//            Date date = format.parse(userViewDTO.getDateOfBirth());
//
//            dto.setAddress(userViewDTO.getAddress());
//            dto.setEmail(userViewDTO.getEmail());
//            dto.setName(userViewDTO.getName());
//            dto.setTelephone(userViewDTO.getTelephone());
//            dto.setDateOfBirth(date);
//            dto.setRole(RoleBuilder.generateDTOFromId(Integer.parseInt(userViewDTO.getRole())));
//            if (userViewDTO.getId()!=null)
//                dto.setId(Integer.parseInt(userViewDTO.getId()));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//    return dto;
//
//
//    }
}
