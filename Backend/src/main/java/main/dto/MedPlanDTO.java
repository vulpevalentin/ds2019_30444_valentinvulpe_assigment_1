package main.dto;


import java.util.Date;

public class MedPlanDTO {
    private Integer id;
    private Date dateStart;
    private Date dateEnd;
    private Integer dailyIntake;
    private MedicationDTO medication;
    private PatientDTO patient;

    public MedPlanDTO(Integer id, Date dateStart, Date dateEnd, Integer dailyIntake, MedicationDTO medication, PatientDTO patient) {
        this.id = id;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.dailyIntake = dailyIntake;
        this.medication = medication;
        this.patient = patient;
    }

    public MedPlanDTO(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Integer getDailyIntake() {
        return dailyIntake;
    }

    public void setDailyIntake(Integer dailyIntake) {
        this.dailyIntake = dailyIntake;
    }

    public MedicationDTO getMedication() {
        return medication;
    }

    public void setMedication(MedicationDTO medication) {
        this.medication = medication;
    }

    public PatientDTO getPatient() {
        return patient;
    }

    public void setPatient(PatientDTO patient) {
        this.patient = patient;
    }
}
