package main.dto.View;


import main.dto.UserDTO;

import java.text.SimpleDateFormat;

public class UserViewDTO{

    private String id;
    private String name;
    private String email;
    private String address;
    private String telephone;
    private String dateOfBirth;
    private String role;
    private String caregiver;

    public UserViewDTO(UserDTO userDTO){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        this.id = userDTO.getId().toString();
        this.name = userDTO.getName();
        this.address = userDTO.getAddress();
        this.email = userDTO.getEmail();
        this.dateOfBirth = format.format(userDTO.getDateOfBirth());
        this.telephone = userDTO.getTelephone();
        this.role = userDTO.getRole().getId().toString();
    }

    public UserViewDTO(String id, String name, String email, String address, String telephone, String dateOfBirth, String role, String caregiver) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.address = address;
        this.telephone = telephone;
        this.dateOfBirth = dateOfBirth;
        this.role = role;
        this.caregiver = caregiver;
    }

    public UserViewDTO(String name, String email, String address, String telephone, String dateOfBirth, String role, String caregiver) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.telephone = telephone;
        this.dateOfBirth = dateOfBirth;
        this.role = role;
        this.caregiver = caregiver;
    }

    public UserViewDTO(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public String toString(){
        return
                "Name: " + this.getName()
                        + ", Email:" + this.getEmail()
                        + ", Telephone:" + this.getTelephone()
                        + ", Address:" + this.getAddress()
                        + ", Date of Birth:" + this.getDateOfBirth()
                        + ", Id:" + this.getId()
                        + ", Role:" + this.getRole()
                        + ", Caregiver:" + this.getCaregiver();
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCaregiver() {
        return caregiver;
    }



    public void setCaregiver(String caregiver) {
        this.caregiver = caregiver;
    }


}
