package main.dto.View;


import main.dto.MedicationDTO;

public class MedicationViewDTO {
    private String id;
    private String name;


    public MedicationViewDTO(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public MedicationViewDTO(MedicationDTO medicationDTO){
        this.id = medicationDTO.getId().toString();
        this.name = medicationDTO.getName();
    }

    public MedicationViewDTO(){}

    public MedicationDTO toDTO(){
        if(this.getId() == null)
            return new MedicationDTO(null, this.getName());
        return new MedicationDTO(Integer.parseInt(this.getId()), this.getName());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
