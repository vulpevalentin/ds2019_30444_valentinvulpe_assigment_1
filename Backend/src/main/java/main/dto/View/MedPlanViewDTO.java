package main.dto.View;

import main.dto.MedPlanDTO;

import java.text.SimpleDateFormat;

public class MedPlanViewDTO {




    private String id;
    private String dateStart;
    private String dateEnd;
    private String dailyIntake;
    private String medication;
    private String patient;

    public MedPlanViewDTO(String id, String dateStart, String dateEnd, String dailyIntake, String medication, String patient) {
        this.id = id;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.dailyIntake = dailyIntake;
        this.medication = medication;
        this.patient = patient;
    }
    public MedPlanViewDTO( String dateStart, String dateEnd, String dailyIntake, String medication, String patient) {
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.dailyIntake = dailyIntake;
        this.medication = medication;
        this.patient = patient;
    }
    public MedPlanViewDTO(){}

    public MedPlanViewDTO( MedPlanDTO dto) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        this.id = dto.getId().toString();
        this.dateStart = format.format(dto.getDateStart());
        this.dateEnd = format.format(dto.getDateEnd());
        this.dailyIntake = dto.getDailyIntake().toString();
        this.medication = dto.getMedication().getId().toString();
        this.patient = dto.getPatient().getId().toString();
    }



    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getDailyIntake() {
        return dailyIntake;
    }

    public void setDailyIntake(String dailyIntake) {
        this.dailyIntake = dailyIntake;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}


