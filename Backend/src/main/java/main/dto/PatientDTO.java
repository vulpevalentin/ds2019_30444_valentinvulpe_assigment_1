package main.dto;

import java.io.Serializable;
import java.util.List;

public class PatientDTO implements Serializable {


    private Integer id;
    private UserDTO user;
    private UserDTO caregiver;
    private List<MedPlanDTO> medPlan;


    public PatientDTO(Integer id, UserDTO user, UserDTO caregiver, List<MedPlanDTO> medPlan) {
        this.id = id;
        this.user = user;
        this.caregiver = caregiver;
        this.medPlan = medPlan;
    }

    public PatientDTO(Integer id, UserDTO user, UserDTO caregiver) {
        this.id = id;
        this.user = user;
        this.caregiver = caregiver;
    }

    public PatientDTO( UserDTO user, UserDTO caregiver, List<MedPlanDTO> medPlan) {
        this.user = user;
        this.caregiver = caregiver;
        this.medPlan = medPlan;
    }

    public PatientDTO( UserDTO user, UserDTO caregiver) {
        this.user = user;
        this.caregiver = caregiver;
    }

    public PatientDTO(){}


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public UserDTO getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(UserDTO caregiver) {
        this.caregiver = caregiver;
    }

    public List<MedPlanDTO> getMedPlan() {
        return medPlan;
    }

    public void setMedPlan(List<MedPlanDTO> medPlan) {
        this.medPlan = medPlan;
    }
}
