package main.dto;


import java.util.Date;

public class UserDTO{

    private Integer id;
    private String name;
    private String email;
    private String address;
    private String telephone;
    private Date dateOfBirth;
    private RoleDTO role;

    public UserDTO(Integer id, String name, String email, String address, String telephone, Date dateOfBirth, RoleDTO role) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.address = address;
        this.telephone = telephone;
        this.dateOfBirth = dateOfBirth;
        this.role = role;
    }

    public UserDTO(String name, String email, String address, String telephone, Date dateOfBirth, RoleDTO role) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.telephone = telephone;
        this.dateOfBirth = dateOfBirth;
        this.role = role;
    }

    public UserDTO(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public String toString(){
        return
                "Name: " + this.getName()
                        + ", Email:" + this.getEmail()
                        + ", Telephone:" + this.getTelephone()
                        + ", Address:" + this.getAddress()
                        + ", Date of Birth:" + this.getDateOfBirth().toString()
                        + ", Id:" + this.getId();
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }
}
