package main.dto;


import java.util.List;

public class MedicationDTO {
    private Integer id;
    private String name;
    private List<MedPlanDTO> medPlans;

    public MedicationDTO(Integer id, String name, List<MedPlanDTO> medPlans) {
        this.id = id;
        this.name = name;
        this.medPlans = medPlans;
    }
    public MedicationDTO(String name, List<MedPlanDTO> medPlans) {
        this.name = name;
        this.medPlans = medPlans;
    }
    public MedicationDTO(){}

    public MedicationDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MedPlanDTO> getMedPlans() {
        return medPlans;
    }

    public void setMedPlans(List<MedPlanDTO> medPlans) {
        this.medPlans = medPlans;
    }
}
