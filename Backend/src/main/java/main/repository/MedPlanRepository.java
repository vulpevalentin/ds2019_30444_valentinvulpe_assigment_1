package main.repository;

import main.entity.MedPlan;
import org.springframework.data.repository.CrudRepository;

public interface MedPlanRepository extends CrudRepository<MedPlan, Integer> {
}
