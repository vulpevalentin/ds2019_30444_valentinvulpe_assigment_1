package main.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "MedPlan" )
public class MedPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false)
    private Integer id;

    @Column(name = "dateStart", nullable = false)
    private Date dateStart;

    @Column(name = "dateEnd", nullable = false)
    private Date dateEnd;

    @Column(name = "dailyIntake", nullable = false)
    private Integer dailyIntake;

    @ManyToOne()
    @JoinColumn(name = "Medication_id", nullable = false)
    private Medication medication;

    @ManyToOne()
    @JoinColumn(name = "Patient_id", nullable = false, referencedColumnName = "id")
    private Patient patient;

    public MedPlan(Integer id, Date dateStart, Date dateEnd, Integer dailyIntake, Medication medication, Patient patient) {
        this.id = id;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.dailyIntake = dailyIntake;
        this.medication = medication;
        this.patient = patient;
    }

    public MedPlan(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Integer getDailyIntake() {
        return dailyIntake;
    }

    public void setDailyIntake(Integer dailyIntake) {
        this.dailyIntake = dailyIntake;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
