package main.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Patient")
public class Patient implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false,unique = true)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "User", nullable = false,unique = true, referencedColumnName = "id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "Caregiver", nullable = false, referencedColumnName = "id")
    private User caregiver;

    @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY)
    private List<MedPlan> medPlan;


    public Patient(Integer id, User user, User caregiver, List<MedPlan> medPlan) {
        this.id = id;
        this.user = user;
        this.caregiver = caregiver;
        this.medPlan = medPlan;
    }

    public Patient(Integer id, User user, User caregiver) {
        this.id = id;
        this.user = user;
        this.caregiver = caregiver;
    }

    public Patient( User user, User caregiver, List<MedPlan> medPlan) {
        this.user = user;
        this.caregiver = caregiver;
        this.medPlan = medPlan;
    }

    public Patient( User user, User caregiver) {
        this.user = user;
        this.caregiver = caregiver;
    }

    public Patient(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(User caregiver) {
        this.caregiver = caregiver;
    }

    public List<MedPlan> getMedPlan() {
        return medPlan;
    }

    public void setMedPlan(List<MedPlan> medPlan) {
        this.medPlan = medPlan;
    }
}
