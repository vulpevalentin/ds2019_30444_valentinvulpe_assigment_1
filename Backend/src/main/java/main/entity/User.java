package main.entity;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "User")
public class User{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "telephone", nullable = false)
    private String telephone;

    @Column(name = "dateOfBirth", nullable = false)
    private Date dateOfBirth;

    @ManyToOne()
    @JoinColumn(name = "Role_id", nullable = false)
    private Role role;

    public User(Integer id, String name, String email, String address, String telephone, Date dateOfBirth, Role role) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.address = address;
        this.telephone = telephone;
        this.dateOfBirth = dateOfBirth;
        this.role = role;
    }

    public User(String name, String email, String address, String telephone, Date dateOfBirth, Role role) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.telephone = telephone;
        this.dateOfBirth = dateOfBirth;
        this.role = role;
    }

    public User(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public String toString(){
        return
                "Name: " + this.getName()
                        + ", Email:" + this.getEmail()
                        + ", Telephone:" + this.getTelephone()
                        + ", Address:" + this.getAddress()
                        + ", Date of Birth:" + this.getDateOfBirth().toString()
                        + ", Id:" + this.getId();
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
