package main.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Medication" )
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "medication", fetch = FetchType.LAZY)
    private List<MedPlan> medPlans;

    public Medication(Integer id, String name, List<MedPlan> medPlans) {
        this.id = id;
        this.name = name;
        this.medPlans = medPlans;
    }

    public Medication(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Medication(String name, List<MedPlan> medPlans) {
        this.name = name;
        this.medPlans = medPlans;
    }
    public Medication(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MedPlan> getMedPlans() {
        return medPlans;
    }

    public void setMedPlans(List<MedPlan> medPlans) {
        this.medPlans = medPlans;
    }
}
