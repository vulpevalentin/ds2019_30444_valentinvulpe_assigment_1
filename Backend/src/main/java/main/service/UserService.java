package main.service;

import main.dto.UserDTO;
import main.dto.builder.UserBuilder;
import main.entity.User;
import main.errorhandler.ResourceNotFoundException;
import main.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository repository;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }


    public List<UserDTO> findAll(){
        List<UserDTO> list = new ArrayList<>();
        repository.findAll().forEach((e)->list.add(UserBuilder.generateDTOFromEntity(e)));
        return list;
    }

    public UserDTO findById(int id){
        Optional<User> user = repository.findById(id);
        if (user.isPresent())
            return UserBuilder.generateDTOFromEntity(user.get());
        else
            throw new ResourceNotFoundException("User", "user id", id);
    }

    public Integer save(UserDTO user){
        return repository.save(UserBuilder.generateEntityFromDTO(user)).getId();
    }

    public void delete(Integer id){
        repository.deleteById(id);
    }

    public boolean isPresent(UserDTO userDTO){
        return isPresent(userDTO.getId());
    }

    public boolean isPresent(Integer id){
        if(id == null)
            return false;
        else
            return repository.findById(id).isPresent();
    }




    public Integer update(UserDTO user){
        Optional<User> userOld = repository.findById(user.getId());
        if(userOld.isPresent())
            return repository.save(UserBuilder.generateEntityFromDTO(user)).getId();
        else
            throw new ResourceNotFoundException("User", "user id", user.getId());
    }



}
