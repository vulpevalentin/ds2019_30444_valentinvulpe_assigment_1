package main.service;

import main.dto.RoleDTO;
import main.dto.builder.RoleBuilder;
import main.entity.Role;
import main.errorhandler.ResourceNotFoundException;
import main.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoleService {

    private final RoleRepository repository;

    @Autowired
    RoleService(RoleRepository repository){
        this.repository = repository;
    }

    public List<RoleDTO> findAll(){
        List<RoleDTO> list = new ArrayList<>();
        repository.findAll().forEach((e)->list.add(RoleBuilder.generateDTOFromEntity(e)));
        return list;
    }

    public RoleDTO findById(int id){
        Optional<Role> role = repository.findById(id);
        if(role.isPresent())
            return RoleBuilder.generateDTOFromEntity(role.get());
        else
            throw new ResourceNotFoundException("Role", "id",id);

    }

    public RoleDTO findByName(String name){
        List<RoleDTO> list = this.findAll();
        for (RoleDTO r:list
             ) {
            if(r.getName().equals(name))
                return r;
        }
        throw new ResourceNotFoundException("Role","name", name);
    }

}
