package main.service;

import main.dto.MedPlanDTO;
import main.dto.PatientDTO;
import main.dto.builder.MedPlanBuilder;
import main.entity.MedPlan;
import main.errorhandler.ResourceNotFoundException;
import main.repository.MedPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MedPlanService {
    private final MedPlanRepository repository;

    @Autowired
    MedPlanService(MedPlanRepository repository){
        this.repository = repository;
    }

    public List<MedPlanDTO> findAll(){
        List<MedPlanDTO> list = new ArrayList<>();
        repository.findAll().forEach(u -> list.add(MedPlanBuilder.generateDTOFromEntity(u)));
        return list;
    }

    public MedPlanDTO findById(int id){
        Optional<MedPlan> obj = repository.findById(id);
        if(obj.isPresent())
            return MedPlanBuilder.generateDTOFromEntity(obj.get());
        else
            throw new ResourceNotFoundException("MedPlan", "id", id);
    }

    public List<MedPlanDTO>findByPatient(PatientDTO patientDTO){
        List<MedPlanDTO> allList = findAll();
        List<MedPlanDTO> list = new ArrayList<>();
        for(MedPlanDTO medPlan : allList) {
            if(medPlan.getPatient().getId() == patientDTO.getId())
                list.add(medPlan);
        }
        return list;
    }

    public Integer save(MedPlanDTO dto){
        return repository.save(MedPlanBuilder.generateEntityFromDTO(dto)).getId();
    }

    public void delete(Integer id){
        repository.deleteById(id);
    }

    public boolean isPresent(MedPlanDTO dto){
        return isPresent(dto.getId());
    }

    public boolean isPresent(Integer id){
        if(id == null)
            return false;
        return repository.findById(id).isPresent();
    }







}
