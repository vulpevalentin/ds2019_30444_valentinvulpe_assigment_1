package main.service;

import main.dto.PatientDTO;
import main.dto.UserDTO;
import main.dto.builder.PatientBuilder;
import main.entity.MedPlan;
import main.entity.Patient;
import main.errorhandler.ResourceNotFoundException;
import main.repository.MedPlanRepository;
import main.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PatientService{
    private final PatientRepository repository;

    @Autowired
    PatientService(PatientRepository repository){
        this.repository = repository;
    }

    public List<PatientDTO> findAll(){
        List<PatientDTO> list = new ArrayList<>();
        repository.findAll().forEach(u-> list.add(PatientBuilder.generateDTOFromEntity(u)));
        return list;
    }

    public PatientDTO findById(int id){
        Optional<Patient> obj = repository.findById(id);
        if(obj.isPresent())
            return PatientBuilder.generateDTOFromEntity(obj.get());
        else
            throw new ResourceNotFoundException("Patient", "id", id);
    }

    public Integer save(PatientDTO patientDTO){
        if(isPresentByUser(patientDTO))
        {
            PatientDTO oldDTO = findByUser(patientDTO.getUser());
            oldDTO.setCaregiver(patientDTO.getCaregiver());
            if(!(patientDTO.getMedPlan() == null))
                oldDTO.setMedPlan(patientDTO.getMedPlan());
            return repository.save(PatientBuilder.generateEnityFromDTO(oldDTO)).getId();
        }
        return repository.save(PatientBuilder.generateEnityFromDTO(patientDTO)).getId();
    }

    public void delete(Integer id){
        repository.deleteById(id);
    }

    public boolean isPresent(PatientDTO patientDTO){
        return isPresent(patientDTO.getId());
    }

    public boolean isPresent(Integer id){
        if(id == null)
            return false;
        return repository.findById(id).isPresent();
    }

    public boolean isPresentByUser(PatientDTO patientDTO){
        List<PatientDTO> list = findAll();
        for(PatientDTO listDTO : list){
            if(listDTO.getUser().getId() == patientDTO.getUser().getId())
                return true;
        }
        return false;
    }

    public PatientDTO findByUser(UserDTO userDTO){
        List<PatientDTO> list = findAll();
        for(PatientDTO patientDTO : list){
            if(patientDTO.getUser().getId() == userDTO.getId())
                return patientDTO;
        }
        throw new ResourceNotFoundException("Patient by user", "user id", userDTO.getId());
    }

    public PatientDTO findByUserId(Integer id){
        List<PatientDTO> list = findAll();
        for(PatientDTO patientDTO : list){
            if(patientDTO.getUser().getId() == id)
                return patientDTO;
        }
        throw new ResourceNotFoundException("Patient by user", "user id", id);
    }

    public Integer update(PatientDTO patientDTO){
        Optional<Patient> userOld = repository.findById(patientDTO.getId());
        if(userOld.isPresent())
            return repository.save(PatientBuilder.generateEnityFromDTO(patientDTO)).getId();
        else
            throw new ResourceNotFoundException("Patient", "patientDTO id", patientDTO.getId());
    }

}
