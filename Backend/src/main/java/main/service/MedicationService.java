package main.service;

import main.dto.MedicationDTO;
import main.dto.builder.MedicationBuilder;
import main.entity.Medication;
import main.errorhandler.ResourceNotFoundException;
import main.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MedicationService {
    private final MedicationRepository repository;

    @Autowired
    MedicationService(MedicationRepository medicationRepository){
        this.repository = medicationRepository;
    }

    public List<MedicationDTO> findAll(){
        List<MedicationDTO> list = new ArrayList<>();
        repository.findAll().forEach(u -> list.add(MedicationBuilder.generateDTOFromEntity(u)));
        return list;
    }

    public MedicationDTO findById(int id){
        Optional<Medication> obj = repository.findById(id);
        if(obj.isPresent())
            return MedicationBuilder.generateDTOFromEntity(obj.get());
        else
            throw new ResourceNotFoundException("Medication", "id", id);
    }

    public Integer save(MedicationDTO dto){
            return repository.save(MedicationBuilder.generateEntityFromDTO(dto)).getId();
    }

    public void delete(int id){
        repository.deleteById(id);
    }

    public boolean isPresent(MedicationDTO medication){
        return isPresent(medication.getId());
    }

    public boolean isPresent(Integer id){
        if(id == null)
            return false;
        return repository.findById(id).isPresent();
    }

    public Integer update(MedicationDTO dto){
        return repository.save(MedicationBuilder.generateEntityFromDTO(dto)).getId();
    }

}
