package main.controller;

import main.dto.MedicationDTO;
import main.dto.View.MedicationViewDTO;
import main.service.MedPlanService;
import main.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
//@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@CrossOrigin
@RequestMapping(path = "/medication")
public class MedicationController {
    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService, MedPlanService medPlanService){
        this.medicationService = medicationService;
    }

    @GetMapping("/id")
    public @ResponseBody List<MedicationViewDTO> findByUserId(@RequestParam(name="id") String id){
        //System.out.println("id UYYYYYYYYYYYYY      " + id);
        MedicationDTO dto = medicationService.findById(Integer.parseInt(id));
        List<MedicationViewDTO> list = new ArrayList<>();
        list.add(new MedicationViewDTO(dto));
        //System.out.println(list.get(0).getName());
        return list;
    }

    @GetMapping(path = "/all")
    public @ResponseBody List<MedicationViewDTO> getAll(){
        //System.out.println("/all MEDI\n\n\n\n\n\n");
        List<MedicationViewDTO> list = new ArrayList<>();
        medicationService.findAll().forEach(u -> list.add(new MedicationViewDTO(u)));
       // System.out.println(list.get(0).getName());
        //list.addAll(medicationService.findAll());
        //list.forEach(e -> System.out.println(e.getId() + e.getName()));
        return list;
    }

    @PostMapping(path = "/add")
    public void save(@RequestBody MedicationViewDTO viewDTO){

        MedicationDTO dto = generateDTOFromView(viewDTO);

        System.out.println(dto.getName());
        medicationService.save(dto);

//        MedicationDTO dto2 = medicationService.findById(medicationService.save(dto));
//        List<MedicationViewDTO> list = new ArrayList<>();
//        list.add(new MedicationViewDTO(dto2));
//
//        return list;
    }

    @PostMapping(path = "/del")
    public void delete(@RequestBody MedicationViewDTO viewDTO){
        MedicationDTO dto = generateDTOFromView(viewDTO);
        medicationService.delete(dto.getId());
    }

    private MedicationDTO generateDTOFromView(MedicationViewDTO viewDTO){
        if (viewDTO.getId() == null)
            return new MedicationDTO(null, viewDTO.getName());
        MedicationDTO dto = new MedicationDTO(Integer.parseInt(viewDTO.getId()),viewDTO.getName());
        if(medicationService.isPresent(dto))
            dto.setMedPlans(medicationService.findById(dto.getId()).getMedPlans());
        return dto;
    }
}