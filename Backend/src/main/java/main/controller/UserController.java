package main.controller;

import main.dto.MedPlanDTO;
import main.dto.PatientDTO;
import main.dto.UserDTO;
import main.dto.View.UserViewDTO;
import main.service.MedPlanService;
import main.service.PatientService;
import main.service.RoleService;
import main.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Controller
@CrossOrigin(origins = "http://localhost:4200")
//@CrossOrigin()
@RequestMapping(path = "/user")
public class UserController {

    private final UserService userService;
    private final RoleService roleService;
    private final PatientService patientService;
    private final MedPlanService medPlanService;

    @Autowired
    public UserController(UserService userService, RoleService roleService, PatientService patientService, MedPlanService medPlanService){
        this.userService = userService;
        this.roleService = roleService;
        this.patientService = patientService;
        this.medPlanService = medPlanService;
    }


//    @GetMapping(path = "/id")
//    public String findById (@RequestParam(name="id", required = false, defaultValue = "1")Integer id, Model model){
//        UserDTO user = userService.findById(id);
//        model.addAttribute("name", user.toString());
//        return "greeting";
//    }


//    @GetMapping("/id")
//    public @ResponseBody
//    List<UserViewDTO> findById(@RequestParam(name="id") String id){
//        System.out.println(" xid   " + id);
//        UserDTO userDTO = userService.findById(Integer.parseInt(id));
//        List<UserViewDTO> list = new ArrayList<>();
//        list.add(new UserViewDTO(userDTO));
//        return list;
//    }

    @GetMapping("/id")
    public @ResponseBody List<UserViewDTO> findByUserId(@RequestParam(name="id") String id){
        List<UserViewDTO> list = new ArrayList<>();

        if(!id.equals("null")){
            UserDTO dto = userService.findById(Integer.parseInt(id));

            list.add(new UserViewDTO(dto));
        }


        return list;
    }

    @GetMapping("/caregiver")
    public @ResponseBody List<UserViewDTO> findPatientsByCaregiver(@RequestParam(name="id") String id){

        UserDTO caregiver = userService.findById(Integer.parseInt(id));
        List<UserViewDTO> list = new ArrayList<>();
        List<UserDTO> allList = userService.findAll();
        for(UserDTO dto: userService.findAll()){
            if(dto.getRole().isPatient()){
                PatientDTO patientDTO = patientService.findByUser(dto);
                if(patientDTO.getCaregiver().getId() == caregiver.getId()){
                    list.add(generateViewFromDTO(dto));
                }
            }
        }
        //

        return list;
    }

    @GetMapping(path = "/all")////////////////////////////////////////
    public @ResponseBody List<UserViewDTO> getAll(){
        //System.out.println("/allIJNKUSYHBGDVFTYEGUHJK");
        List<UserViewDTO> list = new ArrayList<>();
        List<UserDTO> userDTOList = userService.findAll();
        for (UserDTO userDTO : userDTOList) {
            UserViewDTO viewDTO = new UserViewDTO(userDTO);
            if(userDTO.getRole().isPatient()){
                PatientDTO patientDTO = patientService.findByUser(userDTO);
                viewDTO.setCaregiver(patientDTO.getCaregiver().getId().toString());
                //System.out.println(patientDTO.getCaregiver().getId());
            }
            list.add(viewDTO);
        }
        return list;
    }

    @GetMapping(path = "/patients")
    public @ResponseBody List<UserViewDTO> getPatients(){
        List<UserViewDTO> list = new ArrayList<>();
        List<UserDTO> userDTOList = userService.findAll();
        for (UserDTO userDTO : userDTOList) {
            if(userDTO.getRole().isPatient()){
//                UserViewDTO viewDTO = new UserViewDTO(userDTO);
//                PatientDTO patientDTO = patientService.findByUser(userDTO);
//                viewDTO.setCaregiver(patientDTO.getCaregiver().getId().toString());
                //System.out.println(patientDTO.getCaregiver().getId());
                UserViewDTO viewDTO = generateViewFromDTO(userDTO);
                list.add(viewDTO);
                }
        }
        //list.forEach(e -> System.out.println(e.getId() + e.getName()));
        return list;
    }

    @GetMapping(path = "/doctors")
    public @ResponseBody List<UserViewDTO> getDoctors(){
        List<UserViewDTO> list = new ArrayList<>();
        userService.findAll().forEach(u -> {
            if(u.getRole().isDoctor())
                list.add(new UserViewDTO(u));
        });
        //System.out.println( "     Doctors \n\n\n");
        return list;
    }

    @GetMapping(path = "/caregivers")
    public @ResponseBody List<UserViewDTO> getCaregivers(){
        List<UserViewDTO> list = new ArrayList<>();
        userService.findAll().forEach(u -> {
            if(u.getRole().isCaregiver())
                list.add(new UserViewDTO(u));
        });
        return list;
    }





    public UserDTO generateDTOFromView(UserViewDTO userViewDTO){
        UserDTO dto = new UserDTO();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = format.parse(userViewDTO.getDateOfBirth());

            dto.setAddress(userViewDTO.getAddress());
            dto.setEmail(userViewDTO.getEmail());
            dto.setName(userViewDTO.getName());
            dto.setTelephone(userViewDTO.getTelephone());
            dto.setDateOfBirth(date);
            dto.setRole(roleService.findById(Integer.parseInt(userViewDTO.getRole())));
            if (userViewDTO.getId()!=null)
                dto.setId(Integer.parseInt(userViewDTO.getId()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dto;
    }

    public UserViewDTO generateViewFromDTO(UserDTO dto){
        UserViewDTO viewDTO = new UserViewDTO(dto);
        if(dto.getRole().isPatient()){
            PatientDTO patientDTO = patientService.findByUser(dto);
            viewDTO.setCaregiver(patientDTO.getCaregiver().getId().toString());
        }
        return viewDTO;
    }

    @PostMapping(path = "/add")
    public void save(@RequestBody UserViewDTO userViewDTO){
            UserDTO userDTO = this.generateDTOFromView(userViewDTO);

            System.out.println(userDTO.getRole().getName());
            Integer id = userService.save(userDTO);
            if(userDTO.getRole().isPatient()){

                Integer caregiver = Integer.parseInt(userViewDTO.getCaregiver());
                PatientDTO patientDTO = new PatientDTO(
                        userService.findById(id),
                        userService.findById(caregiver));
                System.out.println(patientDTO.toString());
                patientService.save(patientDTO);
            }
    }

    @PostMapping(path = "/del")
    public void del(@RequestBody UserViewDTO userViewDTO){
        UserDTO userDTO = this.generateDTOFromView(userViewDTO);
        if(userDTO.getRole().isPatient()){
            PatientDTO patientDTO = patientService.findByUser(userDTO);
            for(MedPlanDTO medPlanDTO : patientDTO.getMedPlan())
                medPlanService.delete(medPlanDTO.getId());
            patientService.delete(patientDTO.getId());
            userService.delete(Integer.parseInt(userViewDTO.getId()));
        }
        else if (userDTO.getRole().isCaregiver()){
            List<PatientDTO> patientDTOList = patientService.findAll();
            patientDTOList.forEach(e -> {
                System.out.println(e.getId() + e.getUser().getName());

            });
                for(PatientDTO patientDTO : patientDTOList){
                    if (patientDTO.getCaregiver().getId() == userDTO.getId()) {
                        System.out.println("    failed   ");
                        return;
                    }
                }
                userService.delete(Integer.parseInt(userViewDTO.getId()));
            }
            else userService.delete(Integer.parseInt(userViewDTO.getId()));
    }

}