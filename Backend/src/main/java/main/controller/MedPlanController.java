package main.controller;

import main.dto.MedPlanDTO;
import main.dto.PatientDTO;
import main.dto.View.MedPlanViewDTO;
import main.service.MedPlanService;
import main.service.MedicationService;
import main.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Controller
//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin
@RequestMapping(path = "/medplan")
public class MedPlanController {
    private final MedPlanService medPlanService;
    private final MedicationService medicationService;
    private final PatientService patientService;

    @Autowired
    public MedPlanController(MedPlanService medPlanService, MedicationService medicationService, PatientService patientService) {
        this.medPlanService = medPlanService;
        this.medicationService = medicationService;
        this.patientService = patientService;
    }

    @GetMapping("/id")
    public @ResponseBody List<MedPlanViewDTO> findByUserId(@RequestParam(name="id") String id){
        //System.out.println("QUU        id   " + id);
        PatientDTO patientDTO = patientService.findByUserId(Integer.parseInt(id));
        List<MedPlanDTO> dtoList = medPlanService.findByPatient(patientDTO);
        //return new MedPlanViewDTO(medPlanDTO);
        List<MedPlanViewDTO> list = new ArrayList<>();
        dtoList.forEach(e -> list.add(new MedPlanViewDTO(e)));
        return list;
    }

    @GetMapping(path = "/all")
    public @ResponseBody List<MedPlanViewDTO> getAll(){
        List<MedPlanViewDTO> list = new ArrayList<>();
        medPlanService.findAll().forEach(u -> list.add(new MedPlanViewDTO(u)));
        return list;
    }

    @PostMapping(path = "/add")
    public void save(@RequestBody MedPlanViewDTO viewDTO){

        viewDTO.setPatient(patientService.findByUserId(Integer.parseInt(viewDTO.getPatient())).getId().toString());
        MedPlanDTO dto = generateDTOFromView(viewDTO);

        System.out.println(dto.getPatient().getUser().getName());
        medPlanService.save(dto);
    }

    @PostMapping(path = "/update")
    public void update(@RequestBody MedPlanViewDTO viewDTO){

       // viewDTO.setPatient(patientService.findByUserId(Integer.parseInt(viewDTO.getPatient())).getId().toString());
        MedPlanDTO dto = generateDTOFromView(viewDTO);

        System.out.println(dto.getPatient().getUser().getName());
        medPlanService.save(dto);
    }

    @PostMapping(path = "/del")
    public void del(@RequestBody MedPlanViewDTO medPlanViewDTO) {
        MedPlanDTO dto = generateDTOFromView(medPlanViewDTO);
        medPlanService.delete(dto.getId());
    }




    private MedPlanDTO generateDTOFromView(MedPlanViewDTO viewDTO){
        MedPlanDTO dto = new MedPlanDTO();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dto.setDateStart(format.parse(viewDTO.getDateStart()));
            dto.setDateEnd(format.parse(viewDTO.getDateEnd()));
            dto.setDailyIntake(Integer.parseInt(viewDTO.getDailyIntake()));
            if(viewDTO.getId() != null)
                dto.setId(Integer.parseInt(viewDTO.getId()));
            dto.setMedication(medicationService.findById(Integer.parseInt(viewDTO.getMedication())));
            dto.setPatient(patientService.findById(Integer.parseInt(viewDTO.getPatient())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dto;
    }

}
