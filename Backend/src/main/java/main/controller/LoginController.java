package main.controller;


import main.LoginUser;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class LoginController {


    @GetMapping(produces = "application/json")
    @RequestMapping({ "/validatelogin" })
    public LoginUser validateLogin() {
        System.out.println("/validateLogin\n\n" );
        return new LoginUser("User successfully authenticated");

    }
}