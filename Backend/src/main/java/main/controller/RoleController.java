package main.controller;

import main.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "/role")
public class RoleController {
    private final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PostMapping(path = "/id")
    public @ResponseBody String findById(@RequestBody String id){
        //System.out.println("/id");
        //System.out.println(id);
        //System.out.println(roleService.findById(Integer.parseInt(id)).getName());
        return roleService.findById(Integer.parseInt(id)).getName();
    }
}
