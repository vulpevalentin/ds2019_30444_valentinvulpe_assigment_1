export class Medplan {
  id: string;
  dateStart: string;
  dateEnd: string;
  dailyIntake: string;
  medication: string;
  patient: string;
  medicationName: string;
}
