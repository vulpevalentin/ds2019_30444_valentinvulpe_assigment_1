export class User {
  id: string;
  name: string;
  email: string;
  address: string;
  telephone: string;
  dateOfBirth: string;
  role: string;
  caregiver: string;
  caregiverName: string;
}
