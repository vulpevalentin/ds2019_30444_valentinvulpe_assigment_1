import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {AppService} from './service/app.service';
import 'rxjs/add/operator/finally';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string;
  listtype: String;
  greeting = {};

  constructor(
    private http: HttpClient,
    private router: Router,
    private app: AppService
  ) {
    //
    // this.title = 'Spring Boot - Angular Application';
    // this.app.authenticate(undefined, undefined);
    // http.get('resource').subscribe(data => this.greeting = data);
  }
  edit(n: String) {
    this.listtype = n;
  }
  // logout() {
  //   this.http.post('logout', {}).finally(() => {
  //     this.app.authenticated = false;
  //     this.router.navigateByUrl('/login');
  //   }).subscribe();
  // }
  authenticated() { return this.app.authenticated; }

}
