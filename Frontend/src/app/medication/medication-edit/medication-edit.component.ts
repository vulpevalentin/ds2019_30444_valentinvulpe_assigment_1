import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MedicationService} from '../../service/medication.service';
import {Medication} from '../../model/medication';

@Component({
  selector: 'app-medication-edit',
  templateUrl: './medication-edit.component.html',
  styleUrls: ['./medication-edit.component.css']
})
export class MedicationEditComponent implements OnInit {
  medication: Medication;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private medicationService: MedicationService
  ) { }

  onSubmit() {
    this.medicationService.save(this.medication).subscribe(data => this.gotoMedicationList());
  }

  gotoMedicationList() {
    this.router.navigate(['/medication/all']);
  }

  ngOnInit() {
    this.medication = this.medicationService.getMedication();
  }
}
