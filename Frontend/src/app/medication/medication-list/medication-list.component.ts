import { Component, OnInit } from '@angular/core';
import {Medication} from '../../model/medication';
import {MedicationService} from '../../service/medication.service';
import {User} from '../../model/user';
import {UserService} from '../../service/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-medication-list',
  templateUrl: './medication-list.component.html',
  styleUrls: ['./medication-list.component.css']
})
export class MedicationListComponent implements OnInit {
  medicationList: Medication[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private medicationService: MedicationService) { }

  ngOnInit() {
    this.medicationService.findAll().subscribe(data => {
      this.medicationList = data;
    });
  }
  gotoMedicationList() {
    this.router.navigate(['/medication/all']);
  }

  delete(medication: Medication) {
    this.medicationService.delete(medication).subscribe(data => this.gotoMedicationList());
  }
  edit(medication: Medication) {
    this.medicationService.edit(medication);
  }
}
