import { Component, OnInit } from '@angular/core';
import {Medication} from '../../model/medication';
import {ActivatedRoute, Router} from '@angular/router';
import {MedicationService} from '../../service/medication.service';

@Component({
  selector: 'app-medication-form',
  templateUrl: './medication-form.component.html',
  styleUrls: ['./medication-form.component.css']
})
export class MedicationFormComponent implements OnInit {

  medication: Medication = new Medication();
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private medicationService: MedicationService
  ) { }

  onSubmit() {
    this.medicationService.save(this.medication).subscribe(data => this.gotoMedicationList());
  }

  gotoMedicationList() {
    this.router.navigate(['../medication/all']);
  }

  ngOnInit() {
  }


}
