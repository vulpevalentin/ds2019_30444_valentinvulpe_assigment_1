import { BrowserModule } from '@angular/platform-browser';
import {Injectable, NgModule} from '@angular/core';


import { AppComponent } from './app.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserFormComponent } from './user/user-form/user-form.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {UserService} from './service/user.service';
import {AppRoutingModule} from './app-routing.module';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { PatientListComponent } from './user/patient-list/patient-list.component';
import { DoctorListComponent } from './user/doctor-list/doctor-list.component';
import { CaregiverListComponent } from './user/caregiver-list/caregiver-list.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { MedicationListComponent } from './medication/medication-list/medication-list.component';
import {MedicationService} from './service/medication.service';
import { MedicationFormComponent } from './medication/medication-form/medication-form.component';
import { MedicationEditComponent } from './medication/medication-edit/medication-edit.component';
import { MedplanEditComponent } from './medplan/medplan-edit/medplan-edit.component';
import { MedplanListComponent } from './medplan/medplan-list/medplan-list.component';
import { MedplanFormComponent } from './medplan/medplan-form/medplan-form.component';
import { PatientViewComponent } from './user/patient-view/patient-view.component';
import { CaregiverViewComponent } from './user/caregiver-view/caregiver-view.component';
import {PatientService} from './service/patient.service';
import {MedplanService} from './service/medplan.service';
import { DoctorViewComponent } from './user/doctor-view/doctor-view.component';
import { LoginComponent } from './login/login.component';
import {AppService} from './service/app.service';
import { LogoutComponent } from './logout/logout.component';
import {AuthGaurdService} from './service/auth-gaurd.service';
import {BasicAuthInterceptorService} from './service/basic-auth-interceptor.service';

// @Injectable()
// export class XhrInterceptor implements HttpInterceptor {
//
//   intercept(req: HttpRequest<any>, next: HttpHandler) {
//     const xhr = req.clone({
//       headers: req.headers.set('X-Requested-With', 'XMLHttpRequest')
//     });
//     return next.handle(xhr);
//   }
// }

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserFormComponent,
    PatientListComponent,
    DoctorListComponent,
    CaregiverListComponent,
    UserEditComponent,
    MedicationListComponent,
    MedicationFormComponent,
    MedicationEditComponent,
    MedplanEditComponent,
    MedplanListComponent,
    MedplanFormComponent,
    PatientViewComponent,
    CaregiverViewComponent,
    DoctorViewComponent,
    LoginComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    UserService,
    MedicationService,
    PatientService,
    MedplanService,
    AppService,
    AuthGaurdService,
    {
      provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptorService, multi: true
    }
    // { provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }


