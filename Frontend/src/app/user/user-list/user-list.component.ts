import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../model/user';
import {UserService} from '../../service/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: User[];
  patientsFlag: boolean;
  constructor(private userService: UserService,
  private route: ActivatedRoute,
  private router: Router) { }

  ngOnInit() {
    this.patientsFlag = false;
      this.userService.findAll().subscribe(data => {
        this.users = data;
      });
  }
  doctorList() {
    this.patientsFlag = false;
    this.userService.findAllDoctors().subscribe(data => {
      this.users = data;
    });
  }
  caregiverList() {
    this.patientsFlag = false;
    this.userService.findAllCaregivers().subscribe(data => {
      this.users = data;
    });
  }
  patientList() {
    this.patientsFlag = true;
    this.userService.findAllPatients().subscribe(data => {
      this.users = data;
    });
  }
  allList() {
    this.patientsFlag = false;
    this.userService.findAll().subscribe(data => {
      this.users = data;
    });
  }
  gotoUserList() {
    this.router.navigate(['/users/all']);
  }
  delete(user: User) {
    this.userService.delete(user).subscribe(data => this.gotoUserList());
  }
  edit(user: User) {
    this.userService.edit(user);
  }
  view(user: User) {
    this.userService.view(user);
  }

  getRole(user: User): String {
    return this.userService.getRole(user.role);
  }

  findCaregiverName(user: User): string {
    if (user.caregiverName == null && user.role === '3') {
      this.userService.findById(user.caregiver).subscribe(data => {
        user.caregiverName = data.pop().name;
      });
    }
    return user.caregiverName;
  }
}


