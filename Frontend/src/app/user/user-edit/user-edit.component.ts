import { Component, OnInit } from '@angular/core';
import {User} from '../../model/user';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../service/user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  user: User; // = new User();
  caregiver: String;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) { }

  onSubmit() {
    this.userService.save(this.user).subscribe(data => this.gotoUserList());
    // this.userService.saveCaregiver(this.caregiver);
  }

  gotoUserList() {
    this.router.navigate(['/users/all']);
  }
  ngOnInit() {
    this.user = this.userService.getUser();
  }

}
