import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../service/user.service';
import {User} from '../../model/user';
import {Medplan} from '../../model/medplan';
import {PatientService} from '../../service/patient.service';
import {MedplanService} from '../../service/medplan.service';
import {MedicationService} from '../../service/medication.service';

@Component({
  selector: 'app-patient-view',
  templateUrl: './patient-view.component.html',
  styleUrls: ['./patient-view.component.css']
})
export class PatientViewComponent implements OnInit {

  user: User; // = new User();
  medPlans: Medplan[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private patientService: PatientService,
    private medplanService: MedplanService,
    private medicationService: MedicationService
  ) { }

  onSubmit() {
    this.userService.save(this.user).subscribe(data => this.gotoUserList());
    // this.userService.saveCaregiver(this.caregiver);
  }

  gotoUserList() {
    this.router.navigate(['/users/all']);
  }
  ngOnInit() {
    this.user = this.userService.getUser();
    this.patientService.findByUser(this.user).subscribe(data => {
      this.medPlans = data;
    });
  }
  edit(medPlan: Medplan) {
    this.medplanService.edit(medPlan);
  }
  delete(medPlan: Medplan) {
    this.medplanService.delete(medPlan).subscribe(data => this.gotoUserList());

  }
  add() {
    this.userService.edit(this.user);
  }

  findMedicationName(medplan: Medplan): string {
    if (medplan.medicationName == null) {
      this.medicationService.findById(medplan.medication).subscribe(data => {
        medplan.medicationName = data.pop().name;
      });
    }
    return medplan.medicationName;
  }



























}
