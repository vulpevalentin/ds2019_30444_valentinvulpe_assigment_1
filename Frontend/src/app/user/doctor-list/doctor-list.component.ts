import { Component, OnInit } from '@angular/core';
import {User} from '../../model/user';
import {UserService} from '../../service/user.service';
import {AppService} from '../../service/app.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css']
})
export class DoctorListComponent implements OnInit {
  title = 'Demo';
  greeting = {};

  users: User[];
  constructor(private app: AppService, private http: HttpClient) {
    // http.get('resource').subscribe(data => this.greeting = data);
  }
  authenticated() { return this.app.authenticated; }

  ngOnInit() {
  }
}
