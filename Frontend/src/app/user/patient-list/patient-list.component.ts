import { Component, OnInit } from '@angular/core';
import {User} from '../../model/user';
import {UserService} from '../../service/user.service';
import {c} from '@angular/core/src/render3';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {
  users: User[];
  caregiver: User;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.findAllPatients().subscribe(data => {
      this.users = data;
    });

    for (const user of this.users) {
      // this.userService.findById(user.caregiver).subscribe(data => {
      //   user.caregiverName = data.pop().name;
      // });
      user.caregiverName = user.name;
    }
  }
  // findById(id: string): string {
  //   this.userService.findById(id).subscribe(data => {
  //     this.caregiver[id] = data;
  //   });
  //   return this.caregiver[id].name;
  // }
  findById(id: string): string {
    // let tem: User[];
    this.userService.findById(id).subscribe(data => {
      this.caregiver = data.pop();
    });
    return this.caregiver.name;
  }

  findById2(id: string): string {
    let tem: User[];
    this.userService.findById(id).subscribe(data => {
      tem = data;
    });
    return tem.pop().name;
  }

  findById3(user: User): string {
    if (user.caregiverName == null) {
    this.userService.findById(user.caregiver).subscribe(data => {
      user.caregiverName = data.pop().name;
    });
    }
    return user.caregiverName;
  }





}
