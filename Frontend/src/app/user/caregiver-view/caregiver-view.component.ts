import { Component, OnInit } from '@angular/core';
import {User} from '../../model/user';
import {UserService} from '../../service/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-caregiver-view',
  templateUrl: './caregiver-view.component.html',
  styleUrls: ['./caregiver-view.component.css']
})
export class CaregiverViewComponent implements OnInit {

  users: User[];
  caregiver: User;
  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.caregiver = this.userService.getUser();
    this.userService.findPatientsByCaregiver(this.caregiver.id).subscribe(data => {
      this.users = data;
    });
  }

  allList() {
    this.userService.findAll().subscribe(data => {
      this.users = data;
    });
  }
  gotoUserList() {
    this.router.navigate(['/users/all']);
  }
  delete(user: User) {
    this.userService.delete(user).subscribe(data => this.gotoUserList());
  }
  edit(user: User) {
    this.userService.edit(user);
  }
  view(user: User) {
    this.userService.view(user);
  }

  getRole(user: User): String {
    return this.userService.getRole(user.role);
  }

  findCaregiverName(user: User): string {
    if (user.caregiverName == null && user.role === '3') {
      this.userService.findById(user.caregiver).subscribe(data => {
        user.caregiverName = data.pop().name;
      });
    }
    return user.caregiverName;
  }
}
