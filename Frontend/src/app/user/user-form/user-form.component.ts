import { Component, OnInit } from '@angular/core';
import {User} from '../../model/user';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../service/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  user: User = new User();
  caregiver: String;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) { }

  onSubmit() {
    this.userService.save(this.user).subscribe(data => this.gotoUserList());
  }

  gotoUserList() {
    this.router.navigate(['../users/all']);
  }
  ngOnInit() {
    this.user.role = '1';
  }

}
