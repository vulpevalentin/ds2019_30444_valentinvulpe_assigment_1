import { Component, OnInit } from '@angular/core';
import {AppService} from '../service/app.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // credentials = {username: '', password: ''};
  username = 'javainuse';
  password = '';
  invalidLogin = false;

  constructor(
    private loginService: AppService,
    private http: HttpClient,
    private router: Router
  ) {
  }

  // login() {
  //   this.app.authenticate(this.credentials, () => {
  //     this.router.navigateByUrl('/');
  //   });
  //   return false;
  // }
  checkLogin() {
    if (this.loginService.authenticate(this.username, this.password)
    ) {
      this.router.navigate(['/user/list/all']);
      this.invalidLogin = false;
    } else {
      this.invalidLogin = true;
    }
  }


  ngOnInit() {
  }

}
