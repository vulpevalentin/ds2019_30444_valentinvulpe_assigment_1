import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedplanListComponent } from './medplan-list.component';

describe('MedplanListComponent', () => {
  let component: MedplanListComponent;
  let fixture: ComponentFixture<MedplanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedplanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedplanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
