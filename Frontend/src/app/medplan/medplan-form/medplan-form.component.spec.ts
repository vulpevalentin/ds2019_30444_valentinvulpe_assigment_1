import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedplanFormComponent } from './medplan-form.component';

describe('MedplanFormComponent', () => {
  let component: MedplanFormComponent;
  let fixture: ComponentFixture<MedplanFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedplanFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedplanFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
