import { Component, OnInit } from '@angular/core';
import {Medplan} from '../../model/medplan';
import {ActivatedRoute, Router} from '@angular/router';
import {MedplanService} from '../../service/medplan.service';
import {UserService} from '../../service/user.service';
import {PatientService} from '../../service/patient.service';
import {User} from '../../model/user';

@Component({
  selector: 'app-medplan-form',
  templateUrl: './medplan-form.component.html',
  styleUrls: ['./medplan-form.component.css']
})
export class MedplanFormComponent implements OnInit {
  medplan: Medplan = new Medplan();
  patient: User;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private patientService: PatientService,
    private medplanService: MedplanService
  ) { }

  ngOnInit() {
    this.patient = this.userService.getUser();
    this.medplan.patient = this.patient.id;
  }

  onSubmit() {
    this.medplanService.save(this.medplan).subscribe(data => this.gotoUserList());
  }

  gotoUserList() {
    this.router.navigate(['/users/all']);
  }

}
