import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MedplanService} from '../../service/medplan.service';
import {Medplan} from '../../model/medplan';

@Component({
  selector: 'app-medplan-edit',
  templateUrl: './medplan-edit.component.html',
  styleUrls: ['./medplan-edit.component.css']
})
export class MedplanEditComponent implements OnInit {
  medplan: Medplan;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private medplanService: MedplanService
    ) { }

  ngOnInit() {
    this.medplan = this.medplanService.getMedplan();
  }

  onSubmit() {
    this.medplanService.update(this.medplan).subscribe(data => this.gotoUserList());
  }

  gotoUserList() {
    this.router.navigate(['/users/all']);
  }

}
