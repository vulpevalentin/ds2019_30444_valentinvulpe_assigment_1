import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Medplan} from '../model/medplan';
import {m} from '@angular/core/src/render3';
import {User} from '../model/user';

@Injectable()
export class MedplanService {
  private medplanUrl: string;
  private medplan: Medplan;

  constructor(private http: HttpClient) {
    this.medplanUrl = 'http://localhost:8080/medplan/';
  }

  public edit(medplan: Medplan) {
    this.medplan = medplan;
  }

  public getMedplan() {
    return this.medplan;
  }

  public save(medplan: Medplan) {
    return this.http.post<Medplan>(this.medplanUrl + 'add' , medplan);
  }
  public update(medplan: Medplan) {
    return this.http.post<Medplan>(this.medplanUrl + 'update' , medplan);
  }
  public delete(medplan: Medplan){
    return this.http.post<Medplan>(this.medplanUrl + 'del' , medplan);
  }
}
