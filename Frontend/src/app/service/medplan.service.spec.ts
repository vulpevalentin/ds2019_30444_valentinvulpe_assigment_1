import { TestBed, inject } from '@angular/core/testing';

import { MedplanService } from './medplan.service';

describe('MedplanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MedplanService]
    });
  });

  it('should be created', inject([MedplanService], (service: MedplanService) => {
    expect(service).toBeTruthy();
  }));
});
