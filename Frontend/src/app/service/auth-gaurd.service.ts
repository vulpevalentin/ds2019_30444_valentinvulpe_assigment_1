import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AppService} from './app.service';

@Injectable()
export class AuthGaurdService implements CanActivate {

  constructor(private router: Router,
              private authService: AppService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigate(['login']);
      return true;
    }
    return true;
    // this.router.navigate(['login']);
    // return false;

  }

}
