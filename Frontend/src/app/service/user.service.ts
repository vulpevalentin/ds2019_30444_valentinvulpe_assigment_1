import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../model/user';
import {Medication} from '../model/medication';

@Injectable()
export class UserService {
  private usersUrl: string;
  private medicationUrl: string;
  private user: User;
  constructor(private http: HttpClient) {
    this.usersUrl = 'http://localhost:8080/user/';
    this.medicationUrl = 'http://localhost:8080/medication/';
  }
  public findAll(): Observable<User[]> {
    console.log('findall');

    // const username = 'javainuse';
    // const password = 'p';
    // const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    // return this.http.get<User[]>(this.usersUrl + 'all', {headers});

    return this.http.get<User[]>(this.usersUrl + 'all');
  }
  public findById(id: string): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl + 'id', {params: {'id': id}});
  }
  public findPatientsByCaregiver(id: string): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl + 'caregiver', {params: {'id': id}});
  }
  public save(user: User) {
    return this.http.post<User>(this.usersUrl + 'add' , user);
  }
  public findAllPatients(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl + 'patients');
  }
  public findAllDoctors(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl + 'doctors');
  }
  public findAllCaregivers(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl + 'caregivers');
  }
  public delete(user: User) {
    alert('delete ' + user.id);
    return this.http.post<User>(this.usersUrl + 'del' , user);
  }
  public edit(user: User) {
    this.user = user;
  }
  public view(user: User) {
    this.user = user;
  }
  public getUser(): User {
    return this.user;
  }
  public getRole(id: String): String {
    // let roleName: Object;
    // return this.http.post<String>('http://localhost:8080/role/id', id); // .subscribe(data => {roleName = data;});
    // return roleName.toString();
    if (id === '1') {
      return 'Doctor';
    }
    if (id === '2') {
      return 'Caregiver';
    }
    if (id === '3') {
      return 'Patient';
    }
  }

}
