import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Medplan} from '../model/medplan';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/user';

@Injectable()
export class PatientService {
  private medPlanUrl: string;

  constructor(private http: HttpClient) {
    this.medPlanUrl = 'http://localhost:8080/medplan/';
  }

  public findAll(): Observable<Medplan[]> {
    return this.http.get<Medplan[]>(this.medPlanUrl + 'all');
  }
  public findByUser(user: User): Observable<Medplan[]> {
    return this.http.get<Medplan[]>(this.medPlanUrl + 'id', {params: {'id': user.id}});
  }
}
