import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Medication} from '../model/medication';
import {User} from '../model/user';

@Injectable()
export class MedicationService {
  private medicationUrl: string;
  private medication: Medication;

  constructor(private http: HttpClient) {
    this.medicationUrl = 'http://localhost:8080/medication/';
  }

  public findAll(): Observable<Medication[]> {
    const username = 'javainuse';
    const password = 'p';
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });

    return this.http.get<Medication[]>('http://localhost:8080/medication/all');
  }
  public save(medication: Medication) {
    return this.http.post<Medication>(this.medicationUrl + 'add', medication);
  }
  public delete(medication: Medication) {
    return this.http.post<Medication>(this.medicationUrl + 'del' , medication);
  }
  public edit(medication: Medication) {
    this.medication = medication;
  }
  public getMedication() {
    return this.medication;
  }
  public findById(id: string): Observable<Medication[]> {
    return this.http.get<Medication[]>(this.medicationUrl + 'id' , {params: {'id': id}});
  }
}
