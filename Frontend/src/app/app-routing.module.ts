import {RouterModule, Routes} from '@angular/router';
import {UserListComponent} from './user/user-list/user-list.component';
import {UserFormComponent} from './user/user-form/user-form.component';
import {NgModule} from '@angular/core';
import {DoctorListComponent} from './user/doctor-list/doctor-list.component';
import {CaregiverListComponent} from './user/caregiver-list/caregiver-list.component';
import {PatientListComponent} from './user/patient-list/patient-list.component';
import {UserEditComponent} from './user/user-edit/user-edit.component';
import {MedicationService} from './service/medication.service';
import {MedicationListComponent} from './medication/medication-list/medication-list.component';
import {MedicationFormComponent} from './medication/medication-form/medication-form.component';
import {MedicationEditComponent} from './medication/medication-edit/medication-edit.component';
import {PatientViewComponent} from './user/patient-view/patient-view.component';
import {MedplanEditComponent} from './medplan/medplan-edit/medplan-edit.component';
import {MedplanFormComponent} from './medplan/medplan-form/medplan-form.component';
import {CaregiverViewComponent} from './user/caregiver-view/caregiver-view.component';
import {DoctorViewComponent} from './user/doctor-view/doctor-view.component';
import {LoginComponent} from './login/login.component';
import {LogoutComponent} from './logout/logout.component';
import {AuthGaurdService} from './service/auth-gaurd.service';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'user/list/all', component: UserListComponent },
  { path: 'user/list/doctors', component: DoctorListComponent },
  { path: 'user/list/caregivers', component: CaregiverListComponent },
  { path: 'user/list/patients', component: PatientListComponent },
  { path: 'user/add', component: UserFormComponent},
  { path: 'user/edit', component: UserEditComponent },
  { path: 'patient/view', component: PatientViewComponent },
  { path: 'doctor/view', component: DoctorViewComponent },
  { path: 'caregiver/view', component: CaregiverViewComponent },
  { path: 'medplan/edit', component: MedplanEditComponent },
  { path: 'medplan/add', component: MedplanFormComponent },
  { path: 'medication/add', component: MedicationFormComponent },
  { path: 'medication/list', component: MedicationListComponent },
  { path: 'medication/edit', component: MedicationEditComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
